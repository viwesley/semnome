<section class="account flex-center">
    <div class="container flex-between">
        <div class="account-content flex-between w-100">
            <aside class="user_left">
                <div class="user_option">
                    <div class="user_photo">
                        <div class="photo">
                            <i class="fas fa-user" style="font-size:60px;"></i>
                        </div>
                        <div class="dados">
                        Olá, Wesley
                        </div>
                    </div>

                    <div class="links-user">
                        <a  href="<?php echo BASE_URL;?>account">
                            <div class="menu-item active_account">
                            Meus dados 
                            </div>
                        </a>

                        <a href="<?php echo BASE_URL;?>pagina/senhaemail.php">
                            <div class="menu-item">
                            Mudar email ou senha
                            </div>
                        </a>

                            <a  href="<?php echo BASE_URL;?>classes/logout.php">
                            <div id="link-logout" class="menu-item">
                            <i class="fas fa-power-off"></i>Sair
                            </div>
                        </a>
                    </div>  
                </div>
            </aside> 
            <section class="account_data">
                <div class="title-medium">
                    <h1>Dados do Usuario</h1>
                </div>
                <div class="">
                    <form action="" method="post">
                        <div class="row">
                            <div class="group-input">
                                <label for="nome">Nome Completo:</label>
                                <input type="text" name="name" class="campo" id="nome" autocomplete="off" value="<?php echo $user['name'] ?>">
                                <div class="erro-input erro-nome-cadastro"></div>
                            </div>

                            <div class="group-input">
                                <label for="emaile">Email:</label>
                                <input type="text" name="email" class="campo" id="emaile" autocomplete="off" value="<?php echo $user['email'] ?>">
                                <div class="erro-input erro-email"></div>
                            </div>

                            <div class="container-input">
                                <strong>Sexo:</strong> <br/>
                                <p>  Masculino</p>
                            </div>
                        </div>

                        <div class="row">  
                            <div class="group-input">
                                <label for="data-nascimento">Data Nascimento:</label>
                                <input type="date" name="date" class="campo no-focus" id="data-nascimento" value="<?php echo $user['date_birth'];?>"autocomplete="off" maxlength="10" readonly="true">
                                <div class="erro-input erro-date"></div>
                            </div>
                        
                            <div class="group-input">
                                <label for="cpf">Cpf:</label>
                                <input type="text" name="cpf" class="campo no-focus" id="cpf" 
                                autocomplete="off" value="<?php echo $user['cpf'];?>" readonly="true">
                                <div class="erro-input erro-cpf"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="group-input">
                                <label for="telefone">Telefone:</label>
                                <input type="text" name="telefone" class="campo" id="telefone" value="<?php echo $user['phone'];?>">
                                <div class="erro-input erro-tel"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="group-input group-submit flex-align-start">
                                <button type="submit" name="cadastro_sub" value="/" class="bnt bnt-cadastro">Altera Dados</button> 
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</section>


<script>
    function inputSexo(obj){
        var obj = obj;
        var div = document.getElementsByClassName("input_sexo");
        div[0].classList.remove("active_sexo");
        div[1].classList.remove("active_sexo");
        obj.classList.add("active_sexo");
    }
</script>