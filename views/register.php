<section id="register" class="flex-center">
    <div class="container flex-center">
        <div class="content_register flex-center flex-column">
            <div class="title title-small flex-center">
                    <h2>Termine seu Cadastro</h2>
                    <h3></h3>
                </div>
            <?php if(!empty($msg)): ?>
                <div class="msg">
                    <p><?php echo($msg);?></p>
                </div>
            <?php endif; ?>
            <form action="" onsubmit="return validarCadastro(this);" method="post" >
                <div class="group-input">
                    <label for="nome">Nome Completo:</label>
                    <input type="text" name="name" class="campo" id="nome" autocomplete="off" value="<?php echo (!empty($name))?$name:''; ?>">
                    <div class="erro-input erro-nome-cadastro"></div>
                </div>

                <div class="group-input flex-row">
                    <div class="container-input">
                        <label for="sexo-masculino">
                            <div onclick="inputSexo(this);" class="input_sexo active_sexo">Masculino</div>
                            <input type="radio" name="sexo" id="sexo-masculino" value='M' class="in" checked>
                        </label>
                        <label for="sexo-feminino">
                            <div onclick="inputSexo(this);" class="input_sexo">Feminino</div>
                            <input type="radio" name="sexo" id="sexo-feminino" value="F" class="in"> 
                        </label> 
                    </div>
                </div>

                <div class="group-input">
                    <label for="data-nascimento">Data Nascimento:</label>
                    <input type="date" name="date" class="campo" id="data-nascimento" autocomplete="off" maxlength="10" >
                    <div class="erro-input erro-date"></div>
                </div>

                <div class="group-input">
                    <label for="cpf">Cpf:</label>
                    <input type="text" name="cpf" class="campo" id="cpf" 
                    autocomplete="off" placeholder="000.000.000-00">
                    <div class="erro-input erro-cpf"></div>
                </div>

                <div class="group-input">
                    <label for="telefone">Telefone:</label>
                    <input type="text" name="telefone" class="campo" id="telefone" autocomplete="off" placeholder="(00)0000-0000">
                    <div class="erro-input erro-tel"></div>
                </div>

                <div class="group-input">
                    <label for="emaile">Email:</label>
                    <input type="text" name="email" class="campo" id="emaile" autocomplete="off" value="<?php echo (!empty($email))?$email:''; ?>">
                    <div class="erro-input erro-email"></div>
                </div>

                <div class="group-input">
                    <label for="senha">Senha:</label>
                    <input type="password" name="senha" class="campo" id="senha">
                    <div class="erro-input erro-password"></div>
                </div>

                <div class="group-input">
                    <label for="senha2">Repita a Senha:</label>
                    <input type="password" name="senha2" class="campo" id="senha2">
                    <div class="erro-input erro-password2"></div>
                </div>

            
                <div class="group-input flex-align-end">
                    <input type="submit" name="bnt-sub-cadastro" value="Cadastrar" class="bnt bnt-cadastro">  
                </div>

            </form>
        </div>
    </div>
</section>


<script>


function validarCadastro(obj){
    var regexNome = /^([A-z\s]{1,})\s([A-z\s]{1,})$/;
    var nome = document.getElementById("nome");
    var date = document.getElementById('data-nascimento');
    var regexCpf = /^([0-9]{3})\.([0-9]{3})\.([0-9]{3})\-([0-9]{2})$/;
    var cpf = document.getElementById('cpf');
    var regexTel= /^\(([0-9]{2})\)([0-9]{1})?([0-9]{4})\-([0-9]{4})$/;
    var tel = document.getElementById('telefone');
    var password = document.getElementById('senha');
    var password2 = document.getElementById('senha2');
    var email = document.getElementById("emaile");
    var regex = /^([A-z0-9\.\-\_]+)(@)([A-z0-9])+\.([a-z]{2,3})(.[a-z]{2,3})?$/g;
    var contErro = 0;

    if(nome.value == ""){
        var msg = document.querySelector('.erro-nome-cadastro');
        msg.innerHTML = "O campo nome é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        nome.classList.add("campo-erro");
        contErro += 1;
    }else if(regexNome.test(nome.value)){
        var msg = document.querySelector('.erro-nome-cadastro');
        msg.setAttribute("style", "opacity:0");
        nome.classList.remove("campo-erro");
    }else{
        var msg = document.querySelector('.erro-nome-cadastro');
        msg.innerHTML = "Digite o nome completo";
        msg.setAttribute("style", "opacity:1");
        nome.classList.add("campo-erro");
        contErro += 1;
    }

    if(cpf.value == ""){
        var msg = document.querySelector('.erro-cpf');
        msg.innerHTML = "O campo Cpf é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        cpf.classList.add("campo-erro");
        contErro += 1;
    }else if(regexCpf.test(cpf.value)){
        var msg = document.querySelector('.erro-cpf');
        msg.setAttribute("style", "opacity:0");
        cpf.classList.remove("campo-erro");
    }else{
        var msg = document.querySelector('.erro-cpf');
        msg.innerHTML = "Digte um Cpf Válido";
        msg.setAttribute("style", "opacity:1");
        cpf.classList.add("campo-erro");
        contErro += 1;
    }

    if(email.value == ""){
        var msg = document.querySelector('.erro-email');
        msg.innerHTML = "O campo email é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        contErro += 1;
    }else if(regex.test(email.value)){
        var msg = document.querySelector('.erro-email');
        msg.setAttribute("style", "opacity:0");
        email.classList.remove("campo-erro");
    }else{
        elementPai =  email.parentNode;
        var msg = document.querySelector('.erro-email');
        msg.innerHTML = "Email inválido";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        contErro += 1;
    }


    if(tel.value == ""){
        var msg = document.querySelector('.erro-tel');
        msg.innerHTML = "O campo Telefone é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        tel.classList.add("campo-erro");
        contErro += 1;
    }else if(regexTel.test(tel.value)){
        var msg = document.querySelector('.erro-tel');
        msg.setAttribute("style", "opacity:0");
        tel.classList.remove("campo-erro");
    }else{
        var msg = document.querySelector('.erro-tel');
        msg.innerHTML = "Digite um Telefone Válido";
        msg.setAttribute("style", "opacity:1");
        tel.classList.add("campo-erro");
        contErro += 1;
    }


    if(password.value == ""){
        var msg = document.querySelector('.erro-password');
        msg.innerHTML = "O campo Senha é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        password.classList.add("campo-erro");
        contErro += 1;
    }else{
        var msg = document.querySelector('.erro-password');
        msg.setAttribute("style", "opacity:0");
        password.classList.remove("campo-erro");
    }

    if(password2.value == ""){
        var msg = document.querySelector('.erro-password2');
        msg.innerHTML = "Os senha são obrigatorio";
        msg.setAttribute("style", "opacity:1");
        password2.classList.add("campo-erro");
        contErro += 1;
    }else if(password.value == password2.value){
        var msg = document.querySelector('.erro-password2');
        msg.setAttribute("style", "opacity:0");
        password2.classList.remove("campo-erro");
    }else{
         elementPai =  password2.parentNode;
        var msg = document.querySelector('.erro-password2');
        msg.innerHTML = "As duas senha devem ser iguais";
        msg.setAttribute("style", "opacity:1");
        password2.classList.add("campo-erro");
        contErro += 1;        
    }
    
    if(contErro > 0){
        return false; 
    }else{
        return true;
    }
}



function inputSexo(obj){
    var obj = obj;
    var div = document.getElementsByClassName("input_sexo");
    div[0].classList.remove("active_sexo");
    div[1].classList.remove("active_sexo");
    obj.classList.add("active_sexo");
}
</script>


