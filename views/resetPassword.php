<section class="esq-senha flex-center">
    <div class="container">
        <div class="content-esq-senha flex-center flex-align-center">
            <div class="item-esq-senha">
                <div class="esqueceu-senha item_content-form flex-center flex-align-center">
                    <div class="title title-small">
                        <h2>Redefina sua senha</h2>
                        <h3>Digite seu email para redefinir senha</h3>
                    </div>
                    <?php if(isset($msg) and !empty($msg)): ?>
                        <div class="msg">
                            <p><?php echo $msg;?></p>
                        </div>
                    <?php endif; ?>
                    <form method="post" onsubmit="return validarDados(this);">
                        <div class="group-input" ">
                            <label for="emaile">Email:</label>
                            <input type="text" name="email" class="campo" id="emaile">
                            <div class="erro-input erro-email"></div>
                        </div>
                        <div class="group-input flex-align-end">
                            <input type="submit" value="Proximo" class="bnt bnt-cadastro">  
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
function validarDados(pbj){   
    var email = document.getElementById("emaile");
    var groupEmail = email.parentNode;
    var regex = /^([A-z0-9\.\-\_]+)(@)([A-z0-9])+\.([a-z]{2,3})(.[a-z]{2,3})?$/g;
    var contErro = 0;

    if(email.value == ""){
        var msg = document.querySelector('.erro-email');
        msg.innerHTML = "O campo email é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        groupEmail.classList.add("error");
        contErro += 1;
    }else if(regex.test(email.value)){
        var msg = document.querySelector('.erro-email');
        email.classList.remove("campo-erro");
        msg.setAttribute("style", "opacity:0");
        groupEmail.classList.remove("error");
    }else{
        elementPai =  email.parentNode;
        var msg = document.querySelector('.erro-email');
        msg.innerHTML = "Email inválido";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        groupEmail.classList.add("error");
        contErro += 1;
    }


    if(contErro > 0){
        return false; 
    }else{
        return true;
    }
}


</script>
