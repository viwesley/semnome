<section class="user_action flex-center">
    <div class="container">
        <div class="content-2 flex-between flex-align-start">
            <div class="item-2">
                <div class="login item_content-form flex-center flex-align-center">
                <div class="title title-small">
                    <h2>Faça seu Login</h2>
                    <h3></h3>
                </div>
                <?php if(isset($msg) and !empty($msg)): ?>
                    <div class="msg">
                        <p><?php print_r($msg);?></p>
                    </div>
                <?php endif; ?>
                    <form method="POST" id="form-contato" onsubmit="return validarLogin(this);">
                        <div class="group-input">
                            <label for="email">Email:</label>
                            <input type="text" name="email" class="campo" id="email-login">
                            <div class="erro-input erro-email-login"></div>
                        </div>
                        <div class="group-input">
                            <label for="senha">Senha:</label>
                            <input type="password" name="senha" class="campo" id="senha">
                            <div class="erro-input erro-input-senha"></div>
                        </div>
                        <div class="group-input flex-align-end">
                            <input type="submit" value="Entrar" name="entrar-sub" class="bnt bnt-entrar">
                        </div>
                        <div class="group-input">
                            <a href="<?php echo BASE_URL."account/resetPassword"?>">Esqueceu sua senha?</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="item-2">
                <div class="cadastro item_content-form flex-center flex-align-center">
                <div class="title title-small">
                    <h2>Faça seu Cadastro</h2>
                    <h3></h3>
                </div>
                <?php if(isset($msg_cadastro) and !empty($msg_cadastro)): ?>
                    <div class="msg">
                        <p><?php print_r($msg_cadastro);?></p>
                    </div>
                <?php endif; ?>
                    <form method="POST" onsubmit="return valideRegister(this);">
                        <div class="group-input">
                            <label for="nome">Nome Completo:</label>
                            <input type="text" name="name" class="campo" id="nome-cadastro">
                            <div class="erro-input erro-nome"></div>
                        </div>
                        <div class="group-input">
                        <label for="email-cadastro">Email:</label>
                            <input type="text" name="email" class="campo" id="email-cadastro">
                            <div class="erro-input erro-email-cadastro"></div>
                        </div>
                        <div class="group-input flex-align-end">
                            <input type="submit" name="cadastro_sub" value="Proximo" class="bnt bnt-cadastro">  
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?php echo BASE_URL ?>assets/js/login.js"></script>


