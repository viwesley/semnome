<section class="reset-password flex-center">
    <div class="container flex-center">
        <div class="content-reset-password flex-center flex-align-center">
            <div class="item-reset-password">
                <div class="reset-password item_content-form flex-center flex-align-center">
                    <div class="title title-small">
                        <h2>Redefina sua Senha</h2>
                        <h3>Digite sua nova senha</h3>
                    </div>
                    <?php if(isset($msg) and !empty($msg)): ?>
                        <div class="msg">
                            <p><?php echo $msg;?></p>
                        </div>
                    <?php endif; ?>
                    <form method="post" onsubmit="return validaSenha(this);">
                        <div class="group-input">
                            <label for="password">Nova senha:</label>
                            <input type="password" name=password class="campo" id="password"> 
                            <div class="erro-input erro-password"></div>
                        </div>

                        <div class="group-input">
                            <label for="cpassword">Repita a senha:</label>
                            <input type="password" name=cpassword class="campo" id="cpassword">
                            <div class="erro-input erro-password2"></div>
                        </div>
                        <div class="group-input flex-align-end">
                            <input type="submit" value="Mudar" name="bnt-submit-reset" class="bnt bnt-reset-senha">  
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<section>


<script>

function validaSenha(obj){
    var password = document.getElementById('password');
    var password2 = document.getElementById('cpassword');
    var contErro = 0;


    if(password.value == ""){
        var msg = document.querySelector('.erro-password');
        msg.innerHTML = "O campo Senha é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        password.classList.add("campo-erro");
        contErro += 1;
    }else{
        var msg = document.querySelector('.erro-password');
        msg.setAttribute("style", "opacity:0");
        password.classList.remove("campo-erro");
    }

    if(password2.value == ""){
        var msg = document.querySelector('.erro-password2');
        msg.innerHTML = "O campo repita a Senha é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        password2.classList.add("campo-erro");
        contErro += 1;
    }else if(password.value == password2.value){
        var msg = document.querySelector('.erro-password2');
        msg.setAttribute("style", "opacity:0");
        password2.classList.remove("campo-erro");
    }else{
         elementPai =  password2.parentNode;
        var msg = document.querySelector('.erro-password2');
        msg.innerHTML = "A Segunda senha tem que ser igual a primeira!";
        msg.setAttribute("style", "opacity:1");
        password2.classList.add("campo-erro");
        contErro += 1;        
    }

    if(contErro > 0){
        return false; 
    }else{
        return true;
    }
}

</script>