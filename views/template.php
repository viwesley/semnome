<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Usuarios</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo BASE_URL?>assets/css/style.css"/>
    </head>
    <body>
    <header class="flex-center">
        <div id="container" class="container flex-between flex-align-center">
            <div class="logo">
              
            </div>
    
            <nav>
                <div class="nav">
                    <a href="<?php echo BASE_URL?>account/register">Cadastro</a>
                    <a href="<?php echo BASE_URL?>login">login</a>
                    <a href="<?php echo BASE_URL?>account/logout">sair</a>
                </div>
            </nav>
        </div>    
    </header>
        <?php
            $this-> loadViewInTemplate($viewName, $viewData);
        ?> 

    </body>

</html>
