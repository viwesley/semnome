<?php

/*
* Classe Usuarios
*
* 
*/

class Usuarios extends model{
    public function adduser($nome,$sexo, $dtNascimento, $cpf, $telefone, $email, $senha, $nsenha){
        if($this->verifyName($nome)){
            if($this->verifyCpf($cpf)){
                if($this->verifyDate($dtNascimento)){
                    if($this->verifyTel($telefone)){
                        if($this->verifyEmail($email)){
                            if($this->verifyNewEmail($email)){
                                if($this->verifySenha($senha, $nsenha)){
                                    $sql= "INSERT INTO users (name, sex, date_birth, cpf, phone, email, password)
                                                        VALUES (:nome, :sexo, :dtN, :cpf, :telefone, :email, :senha)";
                                    $sql= $this->db->prepare($sql);
                                    $sql->bindValue(":nome", $nome);
                                    $sql->bindValue(":sexo", $sexo);
                                    $sql->bindValue(":dtN", $dtNascimento);
                                    $sql->bindValue(":cpf", $cpf);
                                    $sql->bindValue(":telefone", $telefone);
                                    $sql->bindValue(':email', $email);
                                    $sql->bindValue(':senha', md5($senha));
                                    $sql->execute();
                                    if($sql->rowCount() > 0){
                                        $id= $this->db->lastInsertId();
                                        $link_email = BASE_URL.'account/activation/'.md5($id);
                                        echo $link_email;
                                        //$_SESSION['id_login']= $id;
                                        //header('location:'.BASE_URL.'account'); 
                                    }else{
                                        return "Algo deu errado tente mais tarde";
                                    }
                                }else{
                                    return "Digite uma senha Valida";
                                }
                            }else{
                                return "Email já Cadastrado!";
                            }
                        }else{
                            return "Email Inválido!";
                        }
                    }else{
                        return "Digite uma telefone Valido";
                    }
                }else{
                    return "Digite uma Data de Nascimento Válida";
                }
            }else{
                return "Digite um Cpf Válido";
            }
        }else{
            return "Digite o nome Completo";
        }
    }
    
    public function verifyEmail($email){
        $regex='/^([A-z0-9\.\-\_]+)(@)([A-z0-9])+\.([a-z]{2,3})(.[a-z]{2,3})?$/';
        if(preg_match($regex, $email)){
            return true;
        }else{
          return false;
        }
    }
    
    public function verifyTel($tel){
        $regex='/^\(([0-9]{2})\)([0-9]{1})?([0-9]{4})\-([0-9]{4})$/';
        if(preg_match($regex, $tel)){
            return true;
        }else{
          return false;
        }
    }

    public function verifyCpf($cpf){
        $regex='/^([0-9]{3})\.([0-9]{3})\.([0-9]{3})\-([0-9]{2})$/';
        if(preg_match($regex, $cpf)){
            return true;
        }else{
          return false;
        }
    }
  
    public function verifyDate($date){
        $regex='/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/';
        if(preg_match($regex, $date)){
            return true;
        }else{
          return false;
        }
    }

    public function verifyName($nome){
        $regex= '/^([A-z\s]{1,})\s([A-z\s]{1,})$/';
        if(preg_match($regex, $nome)){
            return true;
        }else{
            return false;
        }
    }

    public function verifySenha($senha, $nsenha){
        if($senha == $nsenha){
            return true;
        }else{
            return false;
        }
    }

    public function verifyNewEmail($email){
        $sql="SELECT * FROM users WHERE email= :email";
        $sql= $this->db->prepare($sql);
        $sql->bindValue('email', $email);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return false;  
        }else{
            return true;
        }
    }

    public function activationUser($id){
        $sql="UPDATE users SET status = '1' WHERE MD5(id)= :id";
        $sql= $this->db->prepare($sql);
        $sql->bindValue(':id', $id);
        $sql->execute();

        if($sql->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function getUser(){
        $sql="SELECT * FROM users WHERE id=:id";
        $sql= $this->db->prepare($sql);
        $sql->bindValue(":id", $_SESSION['id_login']);
        $sql->execute();

        if($sql->rowCount() > 0){
            $dados = $sql->fetch();
            return $dados;
        }
    }

    public function setUser(){
        $sql="UPDATE users set name=:nome, email=:email";
        $sql= $this->db->prepare($sql);
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":email", $email);
        $sql->execute();
    }

    public function login ($email, $senha){
        
        if($this->verifyEmail($email)){
            $sql="SELECT * FROM users WHERE email=:email and password=:senha";
            $sql= $this->db->prepare($sql);
            $sql->bindValue(":email", $email);
            $sql->bindValue(":senha", md5($senha));
            $sql->execute();

            if($sql->rowCount() > 0){
                $sql="SELECT * FROM users WHERE email=:email and password=:senha and status= '1'";
                $sql= $this->db->prepare($sql);
                $sql->bindValue(":email", $email);
                $sql->bindValue(":senha", md5($senha));
                $sql->execute();
                if($sql->rowCount() > 0){
                    $dados= $sql->fetch();
                    $_SESSION['id_login']= $dados['id'];
                    header('location:'.BASE_URL.'account'); 
                }else{
                    return "Cadastro não ativado!<br/> Acesse seu email e ative sua conta :)";
                }
            }else{
                return "Email ou senha errado! <br/>";
            }
        }else{
            return "Email Invalido!";
        }
    }


    public function validateLogin(){
        if(isset( $_SESSION['id_login']) and !empty( $_SESSION['id_login'])){
            $sql = "SELECT * FROM users WHERE id=:id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id", $_SESSION['id_login']);
            $sql->execute();
            
            if($sql->rowCount() > 0){
                return true;
            }
        }
        return false;
    }

    public function forgotPassword($email){
        if($this->verifyEmail($email)){
            $sql="SELECT * FROM users WHERE email=:email and status= '1'";
            $sql= $this->db->prepare($sql);
            $sql->bindValue(":email", $email);
            $sql->execute();

            if($sql->rowCount() > 0){
                $dados = $sql->fetch();
                $id = $dados['id']; 
                $hash= md5(time().rand(0, 9999).rand(0, 9999).$id);
                $sql="INSERT INTO users_token (id_user, hash) VALUES (:id, :hash)";
                $sql= $this->db->prepare($sql);
                $sql->bindValue(':id', $id);
                $sql->bindValue(':hash' ,$hash);
                $sql->execute();
                echo BASE_URL."account/ResetNewPasssword/".$hash;
            }else{
               return "Email incorreto! Não existem nenhum cadastro ativo com email";
            }
        }else{
            return "Email invalido!";
        }
    }

    public function resetSenha($token){
        $sql="SELECT * FROM users_token WHERE hash = :hash and used = 0";
        $sql= $this->db->prepare($sql);
        $sql->bindValue(':hash', $token);
        $sql->execute();

        if($sql->rowCount() > 0){
            $dados=$sql->fetch();
            return $dados['id_user'];
        }else{
           return false;
        }
    }

    public function setPassword($id, $password, $cpassword, $token){
        if($this->verifySenha($password, $cpassword)){
            $sql="UPDATE users SET password=:password WHERE id=:id";
            $sql= $this->db->prepare($sql);
            $sql->bindValue(':password', MD5($password));
            $sql->bindValue(':id', $id);
            $sql->execute();
    
            $sql="UPDATE users_token SET used='1' WHERE hash=:hash";
            $sql= $this->db->prepare($sql);
            $sql->bindValue(':hash', $token);
            $sql->execute();

            header('location:'.BASE_URL.'login');
        }else{
            return "As duas senha tem que ser iguais"; 
        }
       

    }
}