function validarLogin(obj){
    var email = document.getElementById("email-login");
    var groupEmail = email.parentNode;
    var regex = /^([A-z0-9\.\-\_]+)(@)([A-z0-9])+\.([a-z]{2,3})(.[a-z]{2,3})?$/g;
    var password = document.getElementById("senha");
    var groupPassword = password.parentNode;
    var contErro = 0;

    if(email.value == ""){
        var msg = document.querySelector('.erro-email-login');
        msg.innerHTML = "O campo email é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        groupEmail.classList.add("error");
        contErro += 1;
    }else if(regex.test(email.value)){
        var msg = document.querySelector('.erro-email-login');
        email.classList.remove("campo-erro");
        msg.setAttribute("style", "opacity:0");
        groupEmail.classList.remove("error");
    }else{
        elementPai =  email.parentNode;
        var msg = document.querySelector('.erro-email-login');
        msg.innerHTML = "Email inválido";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        groupEmail.classList.add("error");
        contErro += 1;
    }

    if(password.value == ""){
        var msg = document.querySelector('.erro-input-senha');
        msg.innerHTML = "O campo senha é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        password.classList.add("campo-erro");
        groupPassword.classList.add("error");
        contErro += 1;
    }else{
        var msg = document.querySelector('.erro-input-senha');
        msg.setAttribute("style", "opacity:0");
        password.classList.remove("campo-erro");
        groupPassword.classList.remove("error");
    }


    if(contErro > 0){
        return false; 
    }else{
        return true;
    }
}


function valideRegister(obs){
    var regexNome = /^([A-z\s]{1,})\s([A-z\s]{1,})$/;
    var nome = document.getElementById("nome-cadastro");
    var groupNome = nome.parentNode;
    var email = document.getElementById("email-cadastro");
    var groupEmail = email.parentNode;
    var regex = /^([A-z0-9\.\-\_]+)(@)([A-z0-9])+\.([a-z]{2,3})(.[a-z]{2,3})?$/g;
    var contErro = 0;

    if(nome.value == ""){
        var msg = document.querySelector('.erro-nome');
        msg.innerHTML = "O campo nome é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        nome.classList.add("campo-erro");
        groupNome.classList.add("error");
        contErro += 1;
    }else if(regexNome.test(nome.value)){
        var msg = document.querySelector('.erro-nome');
        msg.setAttribute("style", "opacity:0");
        nome.classList.remove("campo-erro");
        groupNome.classList.remove("error");
    }else{
        elementPai =  nome.parentNode;
        var msg = document.querySelector('.erro-nome');
        msg.innerHTML = "Digite o nome completo";
        msg.setAttribute("style", "opacity:1");
        nome.classList.add("campo-erro");
        groupNome.classList.add("error");
        contErro += 1;
    }

    if(email.value == ""){
        var msg = document.querySelector('.erro-email-cadastro');
        msg.innerHTML = "O campo email é obrigatorio";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        groupEmail.classList.add("error");
        contErro += 1;
    }else if(regex.test(email.value)){
        var msg = document.querySelector('.erro-email-cadastro');
        email.classList.remove("campo-erro");
        msg.setAttribute("style", "opacity:0");
        groupEmail.classList.remove("error");
    }else{
        elementPai =  email.parentNode;
        var msg = document.querySelector('.erro-email-cadastro');
        msg.innerHTML = "Email inválido";
        msg.setAttribute("style", "opacity:1");
        email.classList.add("campo-erro");
        groupEmail.classList.add("error");
        contErro += 1;
    }

    if(contErro > 0){
        return false; 
    }else{
        return true;
    }
}