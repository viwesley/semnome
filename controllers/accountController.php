<?php

class accountController extends controller{

    public function index(){
        $data = array();
        $u = new Usuarios();
        if(!$u->validateLogin()){
            header('location:'.BASE_URL.'login');
        }

        $data['user'] = $u->getUser();
        $this->loadTemplate("account", $data);
    }

    public function register(){
        $dados = Array();
        if(!empty($_GET['name']) and !empty($_GET['email'])){
            $dados['name'] = $_GET['name'];
            $dados['email'] =  $_GET['email'];
        }
        if(isset($_POST['bnt-sub-cadastro']) and !empty($_POST['bnt-sub-cadastro'])){
            if(!empty($_POST['name']) and !empty($_POST['sexo']) and !empty($_POST['date']) and !empty($_POST['cpf']) and !empty($_POST['telefone']) and !empty($_POST['email']) and !empty($_POST['senha']) and !empty($_POST['senha2'])){
                $nome = addslashes($_POST['name']);
                $sexo = addslashes($_POST['sexo']);
                $dtNascimento = addslashes($_POST['date']);
                $cpf= addslashes($_POST['cpf']);
                $telefone = addslashes($_POST['telefone']);
                $email = addslashes($_POST['email']);
                $senha = addslashes($_POST['senha']);
                $senha2 = addslashes($_POST['senha2']);
                $u = new Usuarios();
                $dados['msg'] = $u->adduser($nome,$sexo, $dtNascimento, $cpf, $telefone, $email, $senha, $senha2);
            }else{
                $dados['msg'] = "Todos os campo devem estar preenchidos!";
            }
        }    
        $this->loadTemplate("register", $dados);
    }

    public function activation($h){
        $dados= Array();
        if(isset($h) and !empty($h)){
            $h = addslashes($h);
            $u = new Usuarios();
            if($u->activationUser($h)){
                $this->loadTemplate("activation", $dados);
            }
        }
        header('location:'.BASE_URL);
    }


    public function resetPassword(){
        $u = new Usuarios();
        if($u->validateLogin()){
            header('location:'.BASE_URL.'account');
        }
        $dados = array();

        if(isset($_POST['email']) and !empty($_POST['email'])){
            $email = addslashes($_POST['email']);
            $u = new Usuarios();
            $dados['msg']=$u->forgotPassword($email);
        }
        $this->loadTemplate('resetPassword', $dados);
    }

    public function ResetNewPasssword($token){
        $u = new Usuarios();
        if($u->validateLogin()){
            header('location:'.BASE_URL.'account');
        }
        $dados = array();

        if(isset($token) and !empty($token)){
            $token= addslashes($token);
            $u = new Usuarios();
            $id= $u->resetSenha($token);
            if(empty($id)){
                header('location:'.BASE_URL);
            }
            if(!empty($_POST['bnt-submit-reset'])){
                if(!empty($_POST['password']) and !empty($_POST['cpassword'])){
                    $password=addslashes($_POST['password']);
                    $cpassword=addslashes($_POST['password']);
                    $dados['msg'] = $u->setPassword($id, $password, $cpassword, $token);
                }else{
                    $dados['msg'] = "Todos os campo devem estar preenchidos!";
                }
            }
            $this->loadTemplate('newPassword', $dados);
        }else{
            header('location:'.BASE_URL.'login');
        }
    }

    public function NewPasssword(){
        if(!empty($_POST['bnt-submit-reset'])){
            if(!empty($_POST['password']) and !empty($_POST['cpassword'])){
                $password=addslashes($_POST['password']);
                $cpassword=addslashes($_POST['password']);
                $dados['msg'] = $u->setPassword($id, $password, $cpassword, $token);
            }else{
                $dados['msg'] = "Todos os campo devem estar preenchidos!";
            }
        }
        $this->loadTemplate('newPassword', $dados);
    }

    public function logout(){
        unset( $_SESSION['id_login']);
        header('location:'.BASE_URL);
    }
}