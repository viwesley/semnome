<?php

class loginController extends controller{

    public function __construct(){
        $u = new Usuarios();
        if($u->validateLogin()){
            header('location:'.BASE_URL.'account');
        }
    }

    public function index(){   
        $dados = array();
        if(isset($_POST['entrar-sub']) and !empty($_POST['entrar-sub'])){
            if(!empty($_POST['email']) and !empty($_POST['senha'])){
                $email = addslashes($_POST['email']);
                $senha = addslashes($_POST['senha']);
                $u = new Usuarios();
                $dados['msg']=$u->login($email, $senha);
            }else{
                $dados['msg']="Todos os campo devem estar preenchidos!";
            }
        }

        
        if(isset($_POST['cadastro_sub']) and !empty($_POST['cadastro_sub'])){
            if(isset($_POST['name']) and !empty($_POST['name']) and isset($_POST['email']) and !empty($_POST['email'])){
                $nome = addslashes($_POST['name']);
                $email = addslashes($_POST['email']);
                    $u = new Usuarios();
                    if($u->verifyName($nome)){
                        if($u->verifyEmail($email)){
                            if($u->verifyNewEmail($email)){
                                header('location:'.BASE_URL.'account/register?name='.$nome.'&email='.$email);
                            }else{
                                $dados['msg_cadastro'] = "Email já cadastrado!";
                            }
                        }else{
                            $dados['msg_cadastro'] = "Digite um Email Válido";
                        }
                    }else{
                        $dados['msg_cadastro'] = "Digite o seu nome Completo!";
                    }
            }else{            
                $dados['msg_cadastro']="Todos os campo devem estar preenchidos!";
            }
        }
        $this->loadTemplate("login", $dados);
    }

    
}